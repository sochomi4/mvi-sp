### Framerate upscaling animovaných videí s cílem zachování ostrosti obrysových černých čar

Vytvořit generátor pro interpolování video rámců, abyste získali vysokofrekvenční zpomalené video. Používejte obě architektury: GAN a U-Net. Porovnejte výsledky.

### Data:
https://drive.google.com/drive/folders/1qTaCtDVfrI64jGvykrqXxkUHzyAQqrqO?usp=sharing

### Spuštění
Vytvořit (spuštěním prepare.ipynb), nebo stáhnout data do složky dat. Spustit jupyter notebook s metodou, kterou chceme otestovat.
